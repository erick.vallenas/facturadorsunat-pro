<?php

namespace App\Core\Services\Dni;

use App\Core\Services\Helpers\Functions;
use App\Core\Services\Models\Person;
use GuzzleHttp\Client;

class CSB
{
    public static function search($number)
    {
        if (strlen($number) !== 8) {
            return [
                'success' => false,
                'message' => 'DNI tiene 8 digitos.'
            ];
        }
        
        $client = new  Client(['base_uri' => 'http://api.csbexpress.pe/']);
        $response = $client->request('GET', 'api/v1/dni/'.$number.'?token=NPWKM2BStmwNDXyu');
        if ($response->getStatusCode() == 200 && $response != "") {
            $json = (object) json_decode($response->getBody()->getContents(), true);
            $data_person = (array) $json;
            if (isset($data_person) && count($data_person) > 0 &&
                strlen($data_person['dni']) >= 8 && $data_person['nombres'] !== '') {
                $person = new Person();
                $person->name = $data_person['apellidoPaterno'].' '.$data_person['apellidoMaterno'].', '.$data_person['nombres'];
                $person->number = $data_person['dni'];
                $person->verification_code = Functions::verificationCode($data_person['dni']);
                $person->first_name = $data_person['apellidoPaterno'];
                $person->last_name = $data_person['apellidoMaterno'];
                $person->names = $data_person['nombres'];
                $person->voting_group = null;

                return [
                    'success' => true,
                    'data' => $person
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Datos no encontrados.'
                ];
            }
        }

        return [
            'success' => false,
            'message' => 'Coneccion fallida.'
        ];
    }
}
?>