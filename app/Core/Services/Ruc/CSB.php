<?php

namespace App\Core\Services\Ruc;

use App\Core\Services\Helpers\Functions;
use App\Core\Services\Models\Company;
use GuzzleHttp\Client;

class CSB
{
    public static function search($number)
    {
        if (strlen($number) !== 11) {
            return [
                'success' => false,
                'message' => 'RUC tiene 11 digitos.'
            ];
        }

        $apiKey = '50GoEJNg461ELWJolQzOb0e64XlfBnkHsGNvkie4C95jQmOFsIAHPFhOEZiE';
        $url = 'https://api.peruapis.com/v1/ruc';
        $document = $number;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('document' => $document),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer ' . $apiKey,
                'Accept: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response);
        $data_company = (array) $json->data;


        if (
            isset($data_company) && count($data_company) > 0 &&
            strlen($data_company['ruc']) >= 11 && $data_company['name'] !== ''
        ) {

            $company = new Company();
            $company->name = $data_company['name'];
            $company->number = $data_company['ruc'];
            $company->trade_name = $data_company['commercial_name'];
            $company->address = $data_company['address'];
            $company->department_id = $data_company['region'];
            $company->province_id = $data_company['province'];
            $company->district_id = $data_company['district'];
            $company->phone = null;

            return [
                'success' => true,
                'data' => $company
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Datos no encontrados.'
            ];
        }
    }
}
